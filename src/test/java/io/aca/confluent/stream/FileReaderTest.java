package io.aca.confluent.stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * author bhumireddy
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context-stream.xml")
public class FileReaderTest {

    @Autowired
    private FileReader fileReader;

    @Autowired
    private StreamReader streamReader;

    @Test
    public void test() throws Exception {
        fileReader.readFileAndSendToTopic();
//        streamReader.processMessages();
//        streamReader.getLatch().await(10000, TimeUnit.MILLISECONDS);
        new StreamProcessorDriver().processMessage();
    }
}
