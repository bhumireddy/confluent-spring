package io.aca.confluent.avro;


import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * author bhumireddy
 */
public class SchemaRegistry {

    private final static MediaType SCHEMA_CONTENT =
            MediaType.parse("application/vnd.schemaregistry.v1+json");


    public static void main(String args[]) throws Exception {
//        System.out.println("{\n" +
//                "  \"schema\": \"" +
//                "  {" +
//                "    \\\"namespace\\\": \\\"com.cloudurable.phonebook\\\"," +
//                "    \\\"type\\\": \\\"record\\\"," +
//                "    \\\"name\\\": \\\"Employee\\\"," +
//                "    \\\"fields\\\": [" +
//                "        {\\\"name\\\": \\\"fName\\\", \\\"type\\\": \\\"string\\\"}," +
//                "        {\\\"name\\\": \\\"lName\\\", \\\"type\\\": \\\"string\\\"}," +
//                "        {\\\"name\\\": \\\"age\\\",  \\\"type\\\": \\\"int\\\"}," +
//                "        {\\\"name\\\": \\\"phoneNumber\\\",  \\\"type\\\": \\\"string\\\"}" +
//                "    ]" +
//                "  }\"" +
//                "}"
//);
        String schemaString = new String(Files.readAllBytes(
                Paths.get(ClassLoader.getSystemResource("user.avsc").toURI())));
        System.out.println(schemaString);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://localhost:8081/subjects/io.aca.avro.user-value/versions")
                .post(RequestBody.create(SCHEMA_CONTENT, schemaString)).build();
        String output = client.newCall(request).execute().body().string();
        System.out.println(output);


    }
}
