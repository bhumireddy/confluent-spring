package io.aca.confluent.avro;

import io.aca.confluent.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

/**
 * author bhumireddy
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context-avro.xml")
public class UserProducerTest {

    @Autowired
    private UserProducer producer;

    @Autowired
    private UserConsumer consumer;

    @Test
    public void test() throws Exception {
        User user = User.newBuilder().setName("bhumireddy").
                setSsn("111111111").setDateOfHire("01012000").setCity("charlotte").build();
        producer.sendMessage(user);
        consumer.getLatch().await(1000, TimeUnit.MILLISECONDS);
        assert consumer.getLatch().getCount() == 0;
    }
}
