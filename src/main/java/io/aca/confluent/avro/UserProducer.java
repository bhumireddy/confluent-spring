package io.aca.confluent.avro;

import io.aca.confluent.model.User;
import org.apache.avro.Schema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * author bhumireddy
 */
public class UserProducer {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Value("#{appProperties['confluent.spring.topic.avro']}")
    private String topic;

    public void sendMessage(User user) {
        Schema.Parser parser = new Schema.Parser();

        ListenableFuture<SendResult> aResult = kafkaTemplate.send(topic, user);
        aResult.addCallback(new ListenableFutureCallback<SendResult>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("fail");
            }

            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("success");
            }
        });
    }
}
