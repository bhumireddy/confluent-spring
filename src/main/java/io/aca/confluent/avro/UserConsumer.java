package io.aca.confluent.avro;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;

import java.util.concurrent.CountDownLatch;

/**
 * author bhumireddy
 */
public class UserConsumer implements AcknowledgingMessageListener {

    private CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void onMessage(ConsumerRecord consumerRecord, Acknowledgment acknowledgment) {
        System.out.println(consumerRecord.value());
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    @Override
    public void onMessage(Object o) {

    }
}
