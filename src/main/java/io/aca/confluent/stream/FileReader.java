package io.aca.confluent.stream;

import io.aca.confluent.model.User;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

/**
 * author bhumireddy
 */
public class FileReader {

    private static final Pattern pattern = Pattern.compile("\\|");
    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Value("#{appProperties['confluent.spring.topic.stream.input']}")
    private String topic;

    public void readFileAndSendToTopic() throws Exception {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("TestData.txt").toURI()))) {
            br.lines().forEach(line -> {
                String[] userString = pattern.split(line);
                User user = User.newBuilder().setName(userString[0]).
                        setSsn(userString[1]).setDateOfHire(userString[2]).setCity(userString[3]).build();
                sendMessage(user);

            });
        }
        ;
    }

    private void sendMessage(User user) {
        ProducerRecord<String, User> record = new ProducerRecord<>(topic, "key1", user);
        ListenableFuture<SendResult> aResult = kafkaTemplate.send(topic, user);
        aResult.addCallback(new ListenableFutureCallback<SendResult>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("fail");
            }

            @Override
            public void onSuccess(SendResult sendResult) {
                sendResult.getRecordMetadata();
                System.out.println("success");
            }
        });
    }
}
