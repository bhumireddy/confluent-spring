package io.aca.confluent.stream;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.TopologyBuilder;

import java.util.Properties;

/**
 * author bhumireddy
 */
public class StreamProcessorDriver {

    public void processMessage() {

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "aca");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put("schema.registry.url", "http://localhost:8081");
//        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, UserAvroSerDe.class);
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());


        TopologyBuilder topologyBuilder = new TopologyBuilder();
        topologyBuilder.addProcessor("source", StreamProcessor::new, "source")
                .addSource("source", new StringDeserializer(), new KafkaAvroDeserializer(), "io.aca.stream.user")
                .addSink("sink1", "io.aca.stream.user.aggregation", new StringSerializer(), new StringSerializer(), "sink1");

        KafkaStreams streams = new KafkaStreams(topologyBuilder, props);
        streams.start();
    }
}
