package io.aca.confluent.stream;

import io.aca.confluent.model.User;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * Stream Readers reads the data from topic and groups the data by hire date
 * and puts the aggregated data in output topic for serialization
 * author bhumireddy
 */
public class StreamReader {

    @Value("#{appProperties['confluent.spring.topic.stream.input']}")
    private String topic;

    @Autowired
    private Map configMap;

    private CountDownLatch latch = new CountDownLatch(1);


    public CountDownLatch getLatch() {
        return latch;
    }


    public void processMessages() {

        final Serde<String> stringSerde = Serdes.String();
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "aca");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put("schema.registry.url", "http://localhost:8081");
//        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, UserAvroSerDe.class);
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

//        props.putAll(configMap);
        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, User> messageStream = builder.stream(topic);
        KTable table = messageStream.map(new KeyValueMapper<String, User, KeyValue<String, User>>() {
            @Override
            public KeyValue<String, User> apply(String s, User user) {
                return new KeyValue<String, User>(user.getDateOfHire().toString(), user);
            }
        }).groupBy(new KeyValueMapper<String, User, Object>() {

            @Override
            public Object apply(String s, User user) {
                return s;
            }
        }).count();
        table.toStream().to("io.aca.stream.user.aggregation", Produced.with(Serdes.String(), Serdes.Long()));
        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        try {
            streams.start();
        } catch (Throwable e) {
            e.printStackTrace();
        }


//        latch.countDown();
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }


}
