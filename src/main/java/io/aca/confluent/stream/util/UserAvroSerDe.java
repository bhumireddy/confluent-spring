package io.aca.confluent.stream.util;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Collections;
import java.util.Map;

/**
 * author bhumireddy
 */
public class UserAvroSerDe<T extends SpecificRecord> implements Serde<T> {

    private final Serde<T> inner;

    /**
     * Constructor used by Kafka Streams.
     */
    public UserAvroSerDe() {
        inner = Serdes.serdeFrom(new UserAvroSerializer<>(), new UserAvroDeserializer<>());
    }

    public UserAvroSerDe(SchemaRegistryClient client) {
        this(client, Collections.emptyMap());
    }

    public UserAvroSerDe(SchemaRegistryClient client, Map<String, ?> props) {
        inner = Serdes.serdeFrom(new UserAvroSerializer<>(client, props), new UserAvroDeserializer<>(client, props));
    }

    @Override
    public Serializer<T> serializer() {
        return inner.serializer();
    }

    @Override
    public Deserializer<T> deserializer() {
        return inner.deserializer();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        inner.serializer().configure(configs, isKey);
        inner.deserializer().configure(configs, isKey);
    }

    @Override
    public void close() {
        inner.serializer().close();
        inner.deserializer().close();
    }

}
