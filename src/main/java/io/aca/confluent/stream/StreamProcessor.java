package io.aca.confluent.stream;

import io.aca.confluent.model.User;
import org.apache.kafka.streams.processor.AbstractProcessor;
import org.apache.kafka.streams.processor.ProcessorContext;

/**
 * author bhumireddy
 */
public class StreamProcessor extends AbstractProcessor<String, User> {

    private ProcessorContext context;

    @Override
    public void process(String s, User user) {
        System.out.println(user);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
        this.context.schedule(10000);
//        summaryStore = (KeyValueStore<String, StockTransactionSummary>) this.context.getStateStore("stock-transactions");
//        Objects.requireNonNull(summaryStore, "State store can't be null");

    }
}
