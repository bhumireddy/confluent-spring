package io.aca.confluent.helloworld;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * @author bhumireddy
 */
public class Producer {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Value("#{appProperties['confluent.spring.topic.helloworld']}")
    private String topic;

    public void sendMessage() {
        ListenableFuture<SendResult> aResult = kafkaTemplate.send(topic, "helloworld");
        aResult.addCallback(new ListenableFutureCallback<SendResult>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("fail");
            }

            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("success");
            }
        });
    }
}
