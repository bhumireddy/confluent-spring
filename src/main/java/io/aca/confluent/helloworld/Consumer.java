package io.aca.confluent.helloworld;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;

import java.util.concurrent.CountDownLatch;

/**
 * author bhumireddy
 */
public class Consumer implements AcknowledgingMessageListener {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    private CountDownLatch latch = new CountDownLatch(1);


    public CountDownLatch getLatch() {
        return latch;
    }

    @Override
    public void onMessage(ConsumerRecord consumerRecord, Acknowledgment acknowledgment) {
        System.out.println(consumerRecord.value());
        latch.countDown();
    }

    @Override
    public void onMessage(Object o) {
        System.out.println(o);
    }
}
